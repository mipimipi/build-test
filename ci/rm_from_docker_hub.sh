#!/usr/bin/env bash

access_token=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${1}'", "password": "'${2}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)
repo_name=$3

shift 3

echo "REPO=$repo_name"

for tag in "$@"; do
    echo "TAG=$tag"
    curl -s -H "Authorization: JWT ${access_token}" -X DELETE https://hub.docker.com/v2/repositories/${repo_name}/tags/${tag}/
done    
